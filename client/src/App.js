import React, { useEffect, useState, useReducer } from 'react';
import './App.css';

const initialState = {messages: [],
                      connected: false,
                      names: {},
                      userId: null};

const reducer = function(prevState, action) {
  let newState = Object.assign({}, prevState);
  switch (action.type) {
  case 'ack':
    return Object.assign(newState, {connected: true,
                                    names: action.names,
                                    userId: action.id});
  case 'setSocket':
    return Object.assign(newState, {socket: action.socket});
  case 'newUser':
    return Object.assign(
      newState,
      {names: Object.assign(
        newState.names, {[action.id]: action.name})});
  case 'newMessage':
    if (newState.socket) {
      newState.socket.send(
        JSON.stringify({type: 'newMessage',
                        content: action.content}));
    }
    return prevState;
  case 'recieveMessage':
    return Object.assign(
      newState,
      {messages: [...newState.messages, action]});
  default:
    console.warn('Unhandled action', action);
    return prevState;
  }
};

const ComposeView = function(props) {
  const [newMessage, setNewMessage] = useState('');
  const onSubmit = (e) => {
    e.preventDefault();
    props.dispatch({type: 'newMessage', content: newMessage});
    setNewMessage('');
  };

  return (<form onSubmit={onSubmit}>
          <input placeholder="Message" value={newMessage}
          onChange={(e) => { setNewMessage(e.target.value); }} />
          <button>Send</button>
          </form>);
};

const MessageView = function(props) {
  return (<li>{props.names[props.message.from]}:
              {props.message.content}</li>);
};

const MessagesView = function(props) {
  return (<ul>
          {props.messages.map(
            (msg) =>
              <MessageView key={msg.id}
                           message={msg}
                           names={props.names}/>)
          }</ul>);
};

function App() {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    const ws = new WebSocket('ws://localhost:8080');
    dispatch({type: 'setSocket', socket: ws});
    ws.addEventListener('open', () => {
      console.log("opened websocket");
    });
    ws.addEventListener('message', (event) => {
      console.log("recieved message", event.data);
      dispatch(JSON.parse(event.data));
    });
    return () => { ws.close(); };
  }, [/* no dependencies -- just call on mount/unmount */]);

  return (
    <div className="App">
      <div className="connection">
        {state.connected ? "Connected" : "Connecting..."}
      </div>
      <div className="UserName">
        {state.names[state.userId]}
      </div>
      <ComposeView dispatch={dispatch} />
      <MessagesView messages={state.messages}
                    names={state.names} />
    </div>
  );
}

export default App;
