const WsServer = require('ws').Server;
const uuid = require('uuid/v4');

const wsServer = new WsServer({port: 8080});

WsServer.prototype.broadcast = function(msg) {
  this.clients.forEach((client) => {
    client.send(JSON.stringify(msg));
  });
};

let names = {};

wsServer.on('connection', (wsClient) => {
  console.log('client connected');
  const userId = uuid();
  names[userId] = userId.slice(0, 5);
  wsServer.broadcast({type: 'newUser',
                      id: userId,
                      name: names[userId]});

  wsClient.on('close', () => {
    console.log('client closed');
    delete names[userId];
  });

  wsClient.on('message', (data) => {
    console.log('client message', data);
    const msg = JSON.parse(data);
    switch (msg.type) {
    case 'newMessage':
      {
        const newMessage = {id: uuid(),
                            from: userId,
                            content: msg.content,
                            type: 'recieveMessage'};
        wsServer.broadcast(newMessage);
      }
      break;
    case 'setName':
      names[userId] = msg.content;
      wsServer.broadcast({type: 'changeName',
                          id: userId,
                          name: names[userId]});
      break;
    default:
      console.log("unknown client message", msg);
    }
  });

  wsClient.send(JSON.stringify({type: 'ack',
                                id: userId,
                                names}));
});
